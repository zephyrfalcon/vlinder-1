# repl.py

import interpreter

class REPL:

    def __init__(self):
        self.intp = interpreter.Interpreter()

    def mainloop(self):
        while True:
            s = input("> ")
            # XXX: this should really be some kind of reader that gets input,
            # parses the next expression, evaluates it, then continues with the
            # rest of the input, if any
            results = self.intp.eval_string(s)
            for result in results:
                print(result.to_string())

if __name__ == "__main__":
    
    repl = REPL()
    repl.mainloop()


