# prelude.v

id: function [x] [x]

not: function [x] [if x [false] [true]]

when: function [cond block] [if cond block []]
unless: function [cond block] [if not cond block []]

ref: function ['name] [getattr %caller name] #[get name]

empty?: function [list] [equal? list []]

map: function [f items] [
    results: []
    for-each x items [
        append results (call (ref f) list [x])
    ]
    results
]

filter: function [pred items] [
    results: []
    for-each x items [
        when (call (ref pred) list [x]) [append results x]
        #when (pred x) [append results x]
    ]
    results
]

# TODO: fold/reduce (left and right?)
# TODO: zip
# TODO: equivalent of Python's enumerate()
# TODO: equivalent of Python's range

inc-by: function ['name value] [
    resetattr %caller name (add value (getattr %caller name))
]
inc: function ['name] [
    resetattr %caller name (add 1 (getattr %caller name))
]

## this is not allowed:
##inc: function ['name] [inc-by name 1]

while: function [cond-block exec-block] [
    loop [
        if do cond-block [do exec-block] [break]
    ]
]

# take a list/block's namespace and create an empty child namespace for it.
make-child-namespace: function [block] [object+parent [] (get-namespace block)]

# for x 1 10 [...]
for: function ['name start stop block] [
    newns: make-child-namespace block
    setattr newns name start
    while [less-or-equal? (getattr newns name) stop] [
        do-with block newns
        resetattr newns name (add 1 getattr newns name)
        # would be cool if we could use inc here... but we can't because 
        # 'name is quoted.
    ]
]

sum: function [numbers] [
    result: 0
    for-each num numbers [inc-by result num]
    result
]

list: function [block] [
    result: []
    # we create a new namespace, based on the list/block's namespace, to create 
    # any new values; these should not appear in the namespace where the block 
    # was created!
    newns: make-child-namespace block
    while [not empty? block] [
        set [value rest] read-next block newns
        block:: rest
        append result value
    ]
    result
]

get-parent: function [obj] [
    getattr+default obj '__parent__ null
]

set-parent: function [obj parent] [
    setattr obj '__parent__ parent
]

del-parent: function [obj] [delattr obj '__parent__]

has-parent?: function [obj] [equal? null (get-parent obj)]

dict: function [block] [
    here: make-child-namespace block
    results: object []
    while [not empty? block] [
        set [key rest] read-next block here
        set [value rest] read-next rest here 
        setattr results key value
        block:: rest
    ]
    # TODO: set subtype to dict?
    del-parent results  # parent lookups are undesirable for a dict
    results
]

any?: function [block] [
    newns: make-child-namespace block
    result: false
    while [not empty? block] [
        set [value rest] read-next block newns
        if value [
            result:: true
            break
            # we do have 'return' now but it doesn't work as it should yet...
        ] [block:: rest]
    ]
    result
]

all?: function [block] [
    newns: make-child-namespace block
    result: true
    while [not empty? block] [
        set [value rest] read-next block newns
        if value [block:: rest] [
            result:: false
            break
        ]
    ]
    result
]

case: function [block] [
    newns: make-child-namespace block
    while [not empty? block] [
        set [cond rest] read-next block newns
        set [value rest] read-next rest newns  # must be a block
        # TODO: make sure thatt this is a block
        if cond [return do value] [block:: rest]
    ]
]

# simple implementation of enum. starts counting at 0.
# colors: enum [red orange yellow green blue purple]
# colors/yellow  => 2  # etc
enum: function [block] [
    values: object []
    count: 0
    for-each name block [
        setattr values name count    # need to use setattr here, name is a var
        inc count
    ]
    values
]

# get all the local names defined in the current namespace (from which 'words' 
# is called, that is).
words: function [] [get-names %caller]

#
# type predicates

# Since these functions can accept any value, we need to suppress potential
# function calls by using `val`. Otherwise we would get an error if we called,
# for example, `integer? ref id`.

integer?: function [x] [equal? type val x <integer>]
string?: function [x] [equal? type val x <string>]
symbol?: function [x] [equal? type val x <symbol>]
bfunction?: function [x] [equal? type val x <bfunction>]
ufunction?: function [x] [equal? type val x <ufunction>]
null?: function [x] [equal? type val x <null>]
boolean?: function [x] [equal? type val x <boolean>]
type?: function [x] [equal? type val x <type>]
object?: function [x] [equal? type val x <object>]
list?: function [x] [equal? type val x <list>]
block?: list?

function?: function [x] [any? [bfunction? val x 
                               ufunction? val x]]

