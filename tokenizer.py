# tokenizer.py

import re
import string

re_string = re.compile(r'^(".*?")')
# simple but flawed string representation; good enough for now

def tokenize(s):
    tokens = []
    current_token = ""
    while s:

        # parentheses are ignored altogether! they don't need to match either!
        if s[0] in "()":
            if current_token:
                tokens.append(current_token)
                current_token = ""
            s = s[1:]
            continue

        if current_token:
            if s[0] in string.whitespace:
                tokens.append(current_token)
                current_token = ""
                s = s[1:]
            elif s[0] in ["[", "]"]:
                tokens.append(current_token)
                current_token = ""
                tokens.append(s[0])
                s = s[1:]
            else:
                current_token += s[0]
                s = s[1:]
        else:
            m = re_string.match(s)
            if m:
                tokens.append(m.group(1))
                s = s[len(m.group(1)):]
                continue

            if s[0] == "#":
                # everything up to the next newline is considered a comment
                idx = s.find("\n")
                if idx == -1:
                    s = ""
                else:
                    s = s[idx:]
            elif s[0] in string.whitespace:
                s = s[1:]
            elif s[0] in ["[", "]"]:
                tokens.append(s[0])
                s = s[1:]
            else:
                current_token = s[0]
                s = s[1:]

    # if we're currently processing a token, add it; we're done
    if current_token:
        tokens.append(current_token)

    return tokens


if __name__ == "__main__":
    print(tokenize("the quick brown fox"))
    print(tokenize("if this [a b] [c d]"))
