" vim syntax file for Vlinder-1

if exists("b:current_syntax")
    finish
endif

" keywords
" Vlinder doesn't really have keywords, but we can highlight important words.
syn keyword vlinderKeywords function for-each if loop getattr setattr resetattr
syn keyword vlinderKeywords delattr get set reset del do object

" matches
syn match vlinderComment '#.*$'
" syn match vlinderDefinition '\!\S\+'
syn match vlinderString '\".*\"'
" syn match vlinderVariable '\$\S\+'
syn match vlinderAssignment "\w+:"

" regions
" syn region vlinderCodeBlock start="{" end="}" fold transparent
" syn region vlinderList start="[" end="]" fold transparent

let b:current_syntax = "vlinder"

hi def link vlinderComment Comment
hi def link vlinderKeywords Identifier
hi def link vlinderDefinition Type
hi def link vlinderString String
hi def link vlinderVariable PreProc
hi def link vlinderAssignment Statement

