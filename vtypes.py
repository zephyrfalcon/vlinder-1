# vtypes.py

import parser

class VType:
    # abstract base class

    _type = None
    # override in subclasses; actual values are set by the interpreter.

    # these methods assume that self.value exists, which may not be true for
    # all subclasses. override where necessary.
    def __hash__(self):
        return hash(self.value)
    def __eq__(self, other):
        if isinstance(other, self.__class__):  # hmm =/
            return self.value == other.value
        else:
            return False
    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.value < other.value
        else:
            return self.__class__.__name__ < other.__class__.__name__
    def __le__(self, other):
        return self.__eq__(other) or self.__lt__(other)

    # override in subclasses that can be used as a key for objects
    def is_valid_key(self):
        return False

    def get_type(self):
        if self._type is None:
            raise NotImplementedError("type of %s" % self)
        else:
            return self._type

class VObject(VType):

    def __init__(self, parent=None):
        #self.parent = parent # XXX should really be a special key ne?
        self.data = {}
        if parent is not None:
            assert isinstance(parent, VObject)
            self.set(VSymbol("__parent__"), parent)

    def set(self, key, value):
        assert key.is_valid_key(), "not a valid key"
        self.data[key] = value
    def get(self, key, default=None):
        try:
            value, obj = self.get2(key)
        except KeyError:
            if default is None:
                raise
            value = NULL if default is None else default
        return value
    def update(self, key, value):
        old_value, obj = self.get2(key)
        obj.set(key, value)
    def get2(self, key):
        try:
            value = self.data[key]
        except KeyError:
            if key == VSymbol("__parent__"): raise
            parent = self.get_parent()
            if parent is not None:
                return parent.get2(key)
            raise
        else:
            return value, self
    def delete(self, key):
        del self.data[key]

    def get_keys(self, include_parents=False, include_hidden=False):
        keys = list(self.data.keys())
        if include_parents:
            parent = self.get_parent()
            if parent is not None:
                keys.extend(parent.get_keys(include_parents, include_hidden))
                keys = list(set(keys)) # only unique items
        if not include_hidden:
            def is_hidden(key):
                return isinstance(key, VSymbol) and key.value.startswith("__")
            keys = [key for key in keys if not is_hidden(key)]
        return sorted(keys)

    def del_parent(self):
        try:
            del self.data[VSymbol("__parent__")]
        except KeyError:
            pass

    def get_parent(self):
        try:
            return self.data[VSymbol("__parent__")]
        except KeyError:
            return None

    def __repr__(self):
        return "VObject(...)"
    def to_string(self):
        keys = sorted(self.get_keys())
        contents = ["%s: %s" % (key.to_string(), self.data[key].to_string()) 
                    for key in keys]
        return "object [" + ' '.join(contents) + "]"

    # equality piggybacks on Python...
    def __eq__(self, other):
        if isinstance(other, self.__class__):  # hmm =/
            return self.data == other.data
        else:
            return False
    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.data < other.data
        else:
            return self.__class__.__name__ < other.__class__.__name__

class VList(VType):
    def __init__(self, values):
        self.values = values
        # lists are also blocks, and they can have an environment associated
        # with them:
        self.env = None
    def __repr__(self):
        return "VList(%r)" % repr(self.values)
    # XXX should this be a valid key? maybe under certain circumstances? (e.g.
    # immutable list)

    # FIXME: equality does not take env into consideration!
    def __eq__(self, other):
        if isinstance(other, self.__class__):  # hmm =/
            return self.values == other.values
        else:
            return False
    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.values < other.values
        else:
            return self.__class__.__name__ < other.__class__.__name__

    def to_string(self):
        contents = [x.to_string() for x in self.values]
        return "[" + ' '.join(contents) + "]"
    def as_object(self, intp):
        """ Treat the list as a block, i.e. a new namespace will be created
            based on the list's namespace, and its contents will be evaluated
            in that namespace, which is then returned. 
            Intended for blocks that define names, e.g. [a: 1 b: add x 3]
        """
        newenv = VObject(self.env)
        intp.eval_stuff(self.values, newenv)
        return newenv

class VString(VType):
    def __init__(self, value, remove_quotes=False):
        # TODO: (un)escape properly
        self.value = value
        if remove_quotes:
            self.value = self.value[1:-1]
        # TODO: proper unescaping
    def is_valid_key(self):
        return True
    def to_string(self):
        # TODO: (un)escape properly
        # currently \n & friends, and double quotes, will not work, AOT
        stuff = ["%s" % c for c in self.value]
        return '"' + ''.join(stuff) + '"'
    @classmethod
    def from_string(cls, s):
        return cls(s, remove_quotes=True)

class VNumber(VType):
    def __init__(self, value):
        self.value = value
    def is_valid_key(self):
        return True
# todo: integer, float, etc

class VInteger(VNumber):
    @classmethod 
    def from_string(cls, s):
        return cls(int(s))
    def __repr__(self):
        return "VInteger(%s)" % self.value
    def to_string(self):
        return str(self.value)

class VSymbol(VType):
    def __init__(self, value):
        self.value = value
    def __repr__(self):
        return "VSymbol(%r)" % self.value
    def to_string(self):
        return self.value
    def is_valid_key(self):
        return True
    
    def is_assignment(self):
        return self.value.endswith(":")
    def is_update(self):
        return self.value.endswith("::")
    def is_function_ref(self):
        return self.value.startswith("&")
    def is_quoted(self):
        return self.value.startswith("'")
    def is_accessor(self):
        return "/" in self.value[1:-1]
    def is_option_call(self):
        return self.value.endswith("*")

    def split_accessor(self):
        parts = [s for s in self.value.split("/")]
        if parts[-1].endswith(":"):
            parts[-1] = parts[-1].rstrip(":")
            #parts[-1] = parts[-1][:-1]
        return [parser.parse_value(p) for p in parts]
        # TODO: check types; only symbols and integers are allowed

class Argument:
    def __init__(self, name, quoted=False):
        if quoted and name.startswith("'"):
            name = name[1:]
        self.name = name
        self.quoted = quoted

class VFunction(VType):
    pass

class VBuiltinFunction(VFunction):
    def __init__(self, f, arity, name=None):
        self.f = f
        self.arity = arity
        self.name = name
        self.args = []  # can be added by function annotation
    def __repr__(self):
        return "VBuiltinFunction(%s)" % (self.name or repr(self.f))
    def call(self, intp, caller_env, options, params):
        return self.f(intp, caller_env, options, *params)
    def to_string(self):
        return "<builtin function>"

class VUserDefinedFunction(VFunction):
    def __init__(self, args, block, name=None):
        self.args = args
        self.block = block
        self.name = name
        self.arity = len(args)
    def call(self, intp, caller_env, options, params):
        assert len(params) == len(self.args), \
          "{2}: incorrect number of arguments; expected {0}, got {1}".format(
                  len(self.args), len(params), self.name)
        # create new environment
        newenv = VObject(self.block.env)
        # bind parameter names to values (in this new env)
        for arg, value in zip(self.args, params):
            newenv.set(VSymbol(arg.name), value)
        newenv.set(VSymbol("%caller"), caller_env)
        newenv.set(VSymbol("%options"), options)
        # evaluate the function body in this new env and return the result(s)
        try:
            results = intp.eval_stuff(self.block.values, newenv)
        except ReturnError as e:
            return e.value
        return results[-1] if results else NULL
    def to_string(self):
        return "<user-defined function>"

class VBoolean(VType):
    def __init__(self, value):
        self.value = value
    def __repr__(self):
        return "VBoolean(%s)" % self.value
    def to_string(self):
        return 'true' if self.value else 'false'
    def is_valid_key(self):
        return True

class VNull(VType):
    def to_string(self):
        return "null"
    def __repr__(self):
        return "VNull()"
    def __hash__(self):
        return hash(None)
    def __eq__(self, other):
        return isinstance(other, self.__class__)
    def is_valid_key(self):
        return True

class VTypeType(VType):
    def __init__(self, name, value):
        self.name = name
        self.value = value
    def __repr__(self):
        return "VType(%s)" % self.name
    def is_valid_key(self):
        return True
    def to_string(self):
        return self.name

# reuse these objects as much as possible
TRUE = VBoolean(True)
FALSE = VBoolean(False)
NULL = VNull()

#
# exceptions

class VlinderError(Exception): pass

class ReturnError(VlinderError):
    def __init__(self, value):
        self.value = value

class VlinderAssertionError(Exception):
    def __init__(self, msg):
        self.msg = msg

####

# decorator
def register(dict, name, arity, args=None):
    def deco(f):
        register_builtin(dict, f, name, arity, args=args)
        return f
    return deco

def register_builtin(dict, f, name, arity, args=None):
    vf = VBuiltinFunction(f, arity, name)
    if args is not None:
        assert len(args) == arity
        vf.args = args  # should be a list of Argument objects
    #print("## registering builtin...", vf)
    dict[name] = vf

