# Vlinder: A quick overview

## Ideas behind the language

Vlinder:

* is dynamically typed
* is homoiconic
* is inspired by Lisp/Scheme, Logo, Rebol/Red, Python, Io, Javascript, Ruby and more

## A quick tour

### Starting the REPL

Run `vlinder` to start a REPL:

```
> 3
3
```

### Function calls

Let's call a function `add`. This function takes two arguments, both numbers:

```
> add 4 5
9
```

Notice that we don't need any special characters or syntax for function calls;
no parentheses.

One of the languages that inspired Vlinder is [Logo](https://en.wikipedia.org/wiki/Logo_(programming_language)). 
Vlinder's way of calling functions is much like Logo's. When it sees a symbol
that evaluates to a function, it figures out how many arguments that function
takes (the *arity*). It then scans the code to collect that many arguments,
evaluates them, and passes them to the function.

In the simple case of `add 4 5`, it finds `add`, notices it has an arity of 2,
then finds two arguments for it (4 and 5) and calls `add` with them, resulting
in a value of 9.

What if we have nested calls? The equivalent of 4+5+6 in Vlinder would be:

```
> add 4 add 5 6
15
# or perhaps
> add add 4 5 6
15
```

In the first case, Vlinder sees the first `add`, wants to collect 2 arguments.
It finds a 4. The next thing it sees is another `add`, which means another
function call. So it resolves that nested call (`add 5 6`), resulting in 11,
which is then passed as the second argument to the first `add`.

We can make this clearer with parentheses:

```
> add 4 (add 5 6)
15
> add (add 4 5) 6
15
```

Notice that parentheses in Vlinder are only used to make things clearer. The
languages does not require them; in fact, it ignores them altogether, much like
whitespace.

### Assignment

### Conditionals

### Loops

### ...


<hr>
<hr>

## Types

### Atomic types

We have the usual suspects:

* integers: 4, -15
* floats: 3.1415, -0.01 *(to be implemented)*
* strings: "hello world"
* symbols: `foo`, `bar:`, `empty?`, etc.
* booleans: `true`, `false`
* `null`

### Lists/blocks

These are actually *the same thing*. Lists *are* blocks! Their syntax looks
like this:

```
numbers: [1 3 5 18 0 12]
names: [guido larry matz]
countries: ["The Netherlands" "Belgium" "Luxemburg"]
empty: []
```

In other words, they are surrounded by square brackets `[ ]`, and can contain
zero or more items, separated by whitespace.

Lists are *also* code blocks. Function definitions, for example, look like
this:

```
function [a b] [add a b]
```

As it happens, `function` is just another built-in function that takes two
lists, one containing the function arguments (called `a` and `b` in this case),
and one containing the function body. `add a b` is a function call, but is not
executed at this point; nor is any of the names `a`, `b` or `add` looked up at
this point. These things happen when we call the function.

### Objects, dictionaries, modules and namespaces

## A few basic features

### Assignment

### Conditionals

### Loops


