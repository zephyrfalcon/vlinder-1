# tools.py

import vtypes

def is_list_of_symbols(vlist):
    return isinstance(vlist, vtypes.VList) \
       and all(isinstance(x, vtypes.VSymbol) for x in vlist.values)

