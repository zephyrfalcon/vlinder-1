# parser.py
# "Parsing" here means that tokens are converted to the appropriate Vlinder
# objects. Lists are matched and converted to VLists containing other objects.
# Literals are converted to numbers, strings, etc. Names are turned into symbols.
# At this point, nothing is evaluated or looked up.

import re
import vtypes

def parse(tokens):
    stuff = []
    while tokens:
        expr, tokens = parse_expr(tokens)
        stuff.append(expr)
    return stuff

def parse_expr(tokens):
    assert tokens
    if tokens[0] == "[":
        stuff = []
        inside = tokens[1:]
        while inside:
            if inside[0] == "]":
                return vtypes.VList(stuff), inside[1:]
            else:
                expr, inside = parse_expr(inside)
                stuff.append(expr)
        raise SyntaxError("unbalanced list")
    elif tokens[0] == "]":
        raise SyntaxError("unbalanced list")
    else:
        return parse_value(tokens[0]), tokens[1:]

re_integer = re.compile("^(-?\d+)$")
re_string = re.compile(r'^(".*?")$')

type_regexen = [
    (re_integer, vtypes.VInteger),
    (re_string, vtypes.VString),
]

def parse_value(value):
    for (regex, vtype) in type_regexen:
        m = regex.match(value)
        if m:
            return vtype.from_string(value)
    # if nothing else matches, it's a symbol
    return vtypes.VSymbol(value)


if __name__ == "__main__":
    print(parse(["a", "b"]))
    print(parse(["a", "b", "[", "f", "1", "]"]))

