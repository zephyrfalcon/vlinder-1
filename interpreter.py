# interpreter.py

import os
#
import parser
import tokenizer
import vtypes
import vbuiltins

class Interpreter:
    
    def __init__(self):
        self.builtin_env = self.load_builtins()
        self.load_prelude()

    def load_builtins(self):
        env = vtypes.VObject()
        for key, value in vbuiltins.builtins.items():
            env.set(vtypes.VSymbol(key), value)
        # true, false, null
        env.set(vtypes.VSymbol('true'), vtypes.TRUE)
        env.set(vtypes.VSymbol('false'), vtypes.FALSE)
        env.set(vtypes.VSymbol('null'), vtypes.NULL)
        env.set(vtypes.VSymbol('builtin-namespace'), env)
        self.load_types(env)
        return env

    def load_types(self, env):
        for name, klass in [
            ('<integer>', vtypes.VInteger),
            ('<string>', vtypes.VString),
            ('<symbol>', vtypes.VSymbol),
            ('<boolean>', vtypes.VBoolean),
            ('<object>', vtypes.VObject),
            ('<list>', vtypes.VList),
            ('<null>', vtypes.VNull),
            ('<bfunction>', vtypes.VBuiltinFunction),
            ('<ufunction>', vtypes.VUserDefinedFunction),
            ('<type>', vtypes.VTypeType),
        ]:
            typeobj = vtypes.VTypeType(name, klass)
            env.set(vtypes.VSymbol(name), typeobj)
            klass._type = typeobj

    def load_prelude(self):
        whereami = os.path.dirname(os.path.abspath(__file__))
        prelude_path = os.path.join(whereami, "prelude.v")
        with open(prelude_path, 'r') as f:
            data = f.read()
        self.eval_string(data, self.builtin_env)

    def eval_string(self, s, env=None):
        env = env or self.builtin_env
        tokens = tokenizer.tokenize(s)
        parts = parser.parse(tokens)

        return self.eval_stuff(parts, env)

    def eval_stuff(self, items, env=None):
        """ Evaluate a list of Vlinder objects. """
        results = []
        while items:
            result, items = self.read_expr(items, env)
            results.append(result)
        return results

    def eval_value(self, x, env):
        if isinstance(x, vtypes.VSymbol):
            value = env.get(x)
            return value
        elif isinstance(x, vtypes.VList):
            x = vtypes.VList(x.values[:])  # shallow copy 
            x.env = env
            return x 
        else:
            return x

    def accessor_get(self, parts, env, root=None):
        """ Look up the values for an accessor, given as a list of VTypes. """
        current = root
        for i, x in enumerate(parts):
            if i == 0:
                # first value; just look up as usual
                current = self.eval_value(x, env)
            else:
                # look up in context of current
                # current must be a VObject or a VList (or a VString, I guess)
                # and x must be a symbol or an integer
                if isinstance(current, vtypes.VObject):
                    assert x.is_valid_key()
                    current = current.get(x)
                elif isinstance(current, vtypes.VList):
                    assert isinstance(x, vtypes.VInteger)
                    current = current.values[x.value]
                else:
                    raise ValueError("cannot lookup %s in %s" % (x, current))

        assert current is not None  # this should not happen
        return current

    def accessor_set(self, parts, value, env, update=False):
        current = None
        for i, x in enumerate(parts):
            if i == 0:
                # first value; just look up as usual
                current = self.eval_value(x, env)
            elif i == len(parts) - 1:
                # last item; this is what we assign to
                if isinstance(current, vtypes.VObject):
                    if update:
                        current.update(x, value)
                    else:
                        current.set(x, value)
                elif isinstance(current, vtypes.VList):
                    # note: update option has no effect on lists
                    assert isinstance(x, vtypes.VInteger)
                    current.values[x.value] = value
                else:
                    raise ValueError("cannot set the value of %x" % current)
            else:
                # look up in context of current
                # current must be a VObject or a VList (or a VString, I guess)
                # and x must be a symbol or an integer
                if isinstance(current, vtypes.VObject):
                    assert x.is_valid_key()
                    current = current.get(x)
                elif isinstance(current, vtypes.VList):
                    assert isinstance(x, vtypes.VInteger)
                    current = current.values[x.value]
                else:
                    raise ValueError("cannot lookup %s in %s" % (x, current))

        return value

    def read_expr(self, parts, env):
        if not parts:
            raise Exception("not enough input")
        first = parts[0]
        head = None

        if isinstance(first, vtypes.VSymbol):
            # quoting has preference over everything else
            if first.is_quoted():
                return vtypes.VSymbol(first.value[1:]), parts[1:]

            elif first.is_accessor():
                acc_parts = first.split_accessor() # will remove ":"
                if first.is_assignment():
                    expr, rest = self.read_expr(parts[1:], env)
                    self.accessor_set(acc_parts, expr, env, update=first.is_update())
                    return expr, rest
                else:
                    head = self.accessor_get(acc_parts, env)
                # should handle lookups, assignment, function refs, quotes (just
                # like individual symbols)

            elif first.is_assignment():
                # read the rest of the expression (the value being assigned)
                expr, rest = self.read_expr(parts[1:], env)
                if first.is_update():
                    env.update(vtypes.VSymbol(first.value[:-2]), expr)
                else:
                    env.set(vtypes.VSymbol(first.value[:-1]), expr)
                return expr, rest

            elif first.is_option_call():
                head = self.eval_value(vtypes.VSymbol(first.value[:-1]), env)

            elif first.is_function_ref():
                f = self.eval_value(vtypes.VSymbol(first.value[1:]), env)
                return f, parts[1:]
    
            else:
                # just a plain old symbol
                head = self.eval_value(first, env)

        else:
            # something else than a symbol
            head = self.eval_value(first, env)

        if isinstance(head, vtypes.VFunction):
            params = []
            rest = parts[1:]
            # find out arity N
            # read next N expressions (and evaluate them)
            for i in range(head.arity):
                if head.args and head.args[i].quoted:
                    subexpr, rest = rest[0], rest[1:]
                else:
                    subexpr, rest = self.read_expr(rest, env)
                params.append(subexpr)
            # do we have optional arguments? if so, read the next value, which
            # should evaluate to a list, which in turn will be treated as a
            # block
            options = vtypes.VObject() # default
            if first.is_option_call():
                subexpr, rest = self.read_expr(rest, env)
                assert isinstance(subexpr, vtypes.VList)
                options = subexpr.as_object(self)
            # apply them to this function
            result = head.call(self, env, options, params)
            return result, rest
        else:
            return head, parts[1:]

