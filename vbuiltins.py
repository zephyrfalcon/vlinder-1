# vbuiltins.py

import vtypes
from vtypes import register
import tools

builtins = {}

# NOTE: All built-in functions have a signature that starts with (intp, env, options,
# ...) so we can access the interpreter object and the environment the function
# is called in.

@register(builtins, 'print', 1)
def v_print(intp, env, options, x):
    if isinstance(x, vtypes.VString):
        print(x.value)
    else:
        print(x.to_string())
    return x

@register(builtins, 'add', 2)
def v_add(intp, env, options, a, b):
    assert isinstance(a, vtypes.VNumber)
    assert isinstance(b, vtypes.VNumber)
    c = a.value + b.value
    # XXX for now, assume an integer
    return vtypes.VInteger(c)

@register(builtins, 'do', 1)
def v_do(intp, env, options, block):
    assert isinstance(block, vtypes.VList)
    # make new namespace with the block's namespace as parent
    ns = vtypes.VObject(block.env)
    # execute the block's code in there
    results = intp.eval_stuff(block.values, ns)
    # return final result
    return results[-1] if results else vtypes.NULL

@register(builtins, 'do-with', 2)
def v_do_with(intp, env, options, block, namespace):
    assert isinstance(block, vtypes.VList)
    assert isinstance(namespace, vtypes.VObject)
    # execute the block's code in the given namespace
    results = intp.eval_stuff(block.values, namespace)
    # return final result
    return results[-1] if results else vtypes.NULL

# NOTE: get, set, etc, take a *symbol*, so usually a quoted value; they DO NOT
# leave arguments unevaluated!
# so write `get 'foo`, etc.
@register(builtins, 'get', 1)
def v_get(intp, env, options, name):
    assert isinstance(name, vtypes.VSymbol)
    if name.is_accessor():
        parts = name.split_accessor()
        return intp.accessor_get(parts, env)
    else:
        return env.get(name)

@register(builtins, 'get+default', 2)
def v_get_default(intp, env, options, name, default):
    try:
        return v_get(intp, env, options, name)
    except KeyError:
        return default

@register(builtins, 'set', 2)
def v_set(intp, env, options, target, value):
    """ set <name> <value>
        set <names> <values>
        Set a variable: set 'x 42   # (quoting is necessary here)
        Set multiple variables at once: set [x y z] [1 2 3]   # (but not here)
    """

    def _set(variable, value, env):
        # TODO: (optional) make sure variable doesn't have trailing ":"
        if variable.is_accessor():
            parts = variable.split_accessor()
            intp.accessor_set(parts, value, env)
        else:
            env.set(variable, value)

    if isinstance(target, vtypes.VSymbol):
        _set(target, value, env)
        return value
    elif isinstance(target, vtypes.VList):
        assert isinstance(value, vtypes.VList)
        assert len(target.values) == len(value.values)
        for name, val in zip(target.values, value.values):
            assert isinstance(name, vtypes.VSymbol)
            _set(name, val, env)
        return value
    else:
        raise ValueError("cannot set a value to %s" % target)

# XXX does it make sense to allow multiple assignments? only if they're all
# updates...
@register(builtins, 'reset', 2)
def v_reset(intp, env, options, name, value):
    assert isinstance(name, vtypes.VSymbol)
    if name.is_accessor():
        parts = name.split_accessor()
        intp.accessor_set(parts, value, env, update=True)
    else:
        old_value, its_env = env.get2(name)
        its_env.set(name, value)
    return value

@register(builtins, 'del', 1)
def v_del(intp, env, options, name):
    assert isinstance(name, vtypes.VSymbol)
    if name.is_accessor():
        raise NotImplementedError("accessor_del")
        #parts = name.split_accessor()
        #return intp.accessor_get(parts, env)
    else:
        env.delete(name)
        return vtypes.NULL

@register(builtins, 'function', 2)
def v_function(intp, env, options, args, body):
    # for now, assume that args is a list of symbols
    assert isinstance(args, vtypes.VList)
    assert all(isinstance(x, vtypes.VSymbol) for x in args.values)
    assert isinstance(body, vtypes.VList)
    fargs = [vtypes.Argument(x.value, quoted=x.is_quoted()) 
             for x in args.values]
    newf = vtypes.VUserDefinedFunction(fargs, body)
    return newf

@register(builtins, 'call', 2)
def v_call(intp, env, options, func, args):
    """ call <function|block> [args...]
        
        call (ref add) [3 4]
        => 7
        call [add %1 %2] [3 4]
        => 7
    """
    assert isinstance(args, vtypes.VList)
    if isinstance(func, vtypes.VFunction):
        return func.call(intp, env, options, args.values)
    elif isinstance(func, vtypes.VList):
        block = func  # for clarity
        newns = vtypes.VObject(block.env)
        # create variables %1, %2, etc, for each of the arguments in the list
        for i in range(len(args.values)):
            name = "%{0}".format(i+1)
            newns.set(vtypes.VSymbol(name), args.values[i])
        # execute the block
        results = intp.eval_stuff(block.values, newns)
        return results[-1] if results else vtypes.NULL
    else:
        raise ValueError("call: first argument must be function or block")

@register(builtins, 'append', 2)
def v_append(intp, env, options, list, x):
    assert isinstance(list, vtypes.VList)
    list.values.append(x)
    return list

@register(builtins, 'object', 1)
def v_object(intp, env, options, block):
    assert isinstance(block, vtypes.VList)
    newns = vtypes.VObject(env) # create new namespace
    # all the stuff inside the block is evaluated into this new namespace,
    # which derives from the block's namespace
    intp.eval_stuff(block.values, newns)
    # but afterwards we delete the parent, so we don't include the world
    #newns.del_parent()
    return newns

@register(builtins, 'object+parent', 2)
def v_object_plus_parent(intp, env, options, block, parent):
    assert isinstance(block, vtypes.VList)
    assert isinstance(parent, vtypes.VObject)
    newns = vtypes.VObject(parent)
    intp.eval_stuff(block.values, newns)
    return newns

@register(builtins, 'length', 1)
def v_length(intp, env, options, x):
    if isinstance(x, vtypes.VList):
        return vtypes.VInteger(len(x.values))
    elif isinstance(x, vtypes.VString):
        return vtypes.VInteger(len(x.value))
    elif isinstance(x, vtypes.VObject):
        return vtypes.VInteger(len(x.data))  # up for debate
    else:
        raise ValueError("cannot take length of this value")

@register(builtins, 'getattr', 2)
def v_getattr(intp, env, options, obj, key):
    if isinstance(obj, vtypes.VObject):
        if key.is_accessor():
            parts = key.split_accessor()
            return intp.accessor_get(parts, env, root=obj)
        else:
            return obj.get(key)
    elif isinstance(obj, vtypes.VList) and isinstance(key, vtypes.VInteger):
        return obj.values[key.value]
    else:
        raise ValueError("cannot take key %s of value %s" % (key, obj))

@register(builtins, 'getattr+default', 3)
def v_getattr_default(intp, env, options, obj, key, default):
    try:
        return v_getattr(intp, env, options, obj, key)
    except KeyError:
        return default

@register(builtins, 'setattr', 3)
def v_setattr(intp, env, options, obj, key, value):
    if isinstance(obj, vtypes.VObject):
        obj.set(key, value)
        return obj
    elif isinstance(obj, vtypes.VList) and isinstance(key, vtypes.VInteger):
        obj.values[key.value] = value
        return obj
    else:
        raise ValueError("cannot set key %s of %s" % (key, obj))

@register(builtins, 'resetattr', 3)
def v_resetattr(intp, env, options, obj, key, value):
    if isinstance(obj, vtypes.VObject):
        old_value, the_env = obj.get2(key)
        the_env.set(key, value)
        return obj
    else:
        return v_setattr(intp, env, options, obj, key,value)

@register(builtins, 'delattr', 2)
def v_delattr(intp, env, options, obj, key):
    if isinstance(obj, vtypes.VObject):
        obj.delete(key)
        return obj
    elif isinstance(obj, vtypes.VList) and isinstance(key, vtypes.VInteger):
        del obj.values[key.value]
        return obj
    else:
        raise ValueError("cannot delete key %s of %s" % (key, obj))

@register(builtins, 'for-each', 3, args=[
    vtypes.Argument('name', quoted=True),
    vtypes.Argument('list'),
    vtypes.Argument('block'),
])
def v_for_each(intp, env, options, name, list, block):
    assert isinstance(list, vtypes.VList)
    assert isinstance(block, vtypes.VList)

    if isinstance(name, vtypes.VSymbol):
        result = vtypes.NULL
        for x in list.values:
            newns = vtypes.VObject(block.env)
            newns.set(name, x)
            results = intp.eval_stuff(block.values, newns)
            if results: 
                result = results[-1]
        return result
    elif tools.is_list_of_symbols(name):
        result = vtypes.NULL
        values = list.values[:]
        while values:
            # NOTE: will currently raise an exception if there aren't enough
            # items left in the list
            newns = vtypes.VObject(block.env)
            for sym in name.values:
                newns.set(sym, values.pop(0))
            results = intp.eval_stuff(block.values, newns)
            if results:
                result = results[-1]
        return result
    else:
        raise ValueError("cannot use for-each with %s" % name)

@register(builtins, 'if', 3)
def v_if(intp, env, options, cond, trueblock, falseblock):
    # for now, let's be strict and only allow booleans
    assert isinstance(cond, vtypes.VBoolean)
    block = trueblock if cond.value else falseblock
    newns = vtypes.VObject(block.env)
    results = intp.eval_stuff(block.values, newns)
    # NOTE: as always, this means that any value you *define* in the block, will
    # not be visible outside of it. unlike in Python, an 'if' block introduces
    # a new scope.
    return results[-1] if results else vtypes.NULL

@register(builtins, 'split', 1)
def v_split(intp, env, options, s):
    assert isinstance(s, vtypes.VString)
    parts = s.value.split()
    objs = [vtypes.VString(p) for p in parts]
    return vtypes.VList(objs)

@register(builtins, 'join', 1)
def v_join(intp, env, options, list):
    assert isinstance(list, vtypes.VList)
    strs = []
    sep = options.get(vtypes.VSymbol('sep'), vtypes.VString(""))
    assert isinstance(sep, vtypes.VString)
    for x in list.values:
        if isinstance(x, vtypes.VString):
            strs.append(x.value)
        else:
            strs.append(x.to_string())
    return vtypes.VString(sep.value.join(strs))

class LoopBreakError(Exception): pass

@register(builtins, 'loop', 1)
def v_loop(intp, env, options, block):
    # infinite loop. does not return anything.
    assert isinstance(block, vtypes.VList)
    newenv = vtypes.VObject(block.env)
    while True:
        try:
            results = intp.eval_stuff(block.values, newenv)
        except LoopBreakError:
            break
    return vtypes.NULL

@register(builtins, 'break', 0)
def v_break(intp, env, options):
    raise LoopBreakError

@register(builtins, 'equal?', 2)
def v_equal(intp, env, options, a, b):
    # Returns true if a equals b, false otherwise.
    # NOTE: Since this is a prototype, the implementation is kind of crude, and
    # relies on the __eq__ methods of the various VType subclasses.
    return vtypes.TRUE if a == b else vtypes.FALSE

@register(builtins, 'less-or-equal?', 2)
def v_le(intp, env, options, a, b):
    return vtypes.TRUE if a <= b else vtypes.FALSE

@register(builtins, 'read-next', 2)
def v_read_next(intp, env, options, block, namespace):
    # NOTE: the expression needs to be evaluated in a namespace; usually this
    # will be a child namespace of the block's namespace. we cannot use
    # block.env itself, since that would directly affect the namespace where
    # the block was created. and creating a new child namespace for each call
    # to read-next is not a good idea either.
    assert isinstance(block, vtypes.VList)
    assert isinstance(namespace, vtypes.VObject)
    result, rest = intp.read_expr(block.values, namespace)
    rest_block = vtypes.VList(rest)
    rest_block.env = block.env  # should this stay the same? for consistency?
    return vtypes.VList([result, rest_block])

@register(builtins, 'get-namespace', 1)
def v_get_namespace(intp, env, options, block):
    assert isinstance(block, vtypes.VList)
    return block.env
    # XXX what if env is None?

# XXX may be renamed/moved; maybe system/id or something? vlinder/id?
@register(builtins, 'internal-id', 1)
def v_internal_id(intp, env, options, x):
    """ Get the internal, unique ID of an object. """
    return vtypes.VInteger(id(x))

@register(builtins, 'set-namespace', 2)
def v_set_namespace(intp, env, options, block, namespace):
    assert isinstance(block, vtypes.VList)
    assert isinstance(namespace, vtypes.VObject)
    block.env = namespace
    return block

@register(builtins, 'symbol?', 1)
def v_symbol_p(intp, env, options, x):
    return vtypes.TRUE if isinstance(x, vtypes.VSymbol) else vtypes.FALSE

@register(builtins, 'get-names', 1)
def v_get_names(intp, env, options, obj):
    assert isinstance(obj, vtypes.VObject)
    include_parents = options.get(vtypes.VSymbol('include-parents'), vtypes.FALSE)
    assert isinstance(include_parents, vtypes.VBoolean)
    include_hidden = options.get(vtypes.VSymbol('include-hidden'), vtypes.FALSE)
    assert isinstance(include_hidden, vtypes.VBoolean)
    keys = obj.get_keys(include_parents=include_parents.value, 
                        include_hidden=include_hidden.value)
    return vtypes.VList(sorted(list(keys)))

@register(builtins, 'current-namespace', 0)
def v_current_namespace(intp, env, options):
    return env

@register(builtins, 'return', 1)
def v_return(intp, env, options, result):
    raise vtypes.ReturnError(result)

@register(builtins, 'assert', 2)
def v_assert(intp, env, options, cond, msg):
    assert isinstance(cond, vtypes.VBoolean)
    if not cond.value:
        raise vtypes.VlinderAssertionError(msg.value)

@register(builtins, 'type', 1)
def v_type(intp, env, options, x):
    """ Return the built-in type of a value. """
    return x.get_type()

# XXX can be written in Vlinder itself?
@register(builtins, 'has-type?', 2)
def v_has_type(intp, env, options, x, type):
    raise NotImplementedError

@register(builtins, 'val', 1, args=[
    vtypes.Argument('x', quoted=True),
])
def v_val(intp, env, options, x):
    """ If x is a symbol, evaluate it and return the value; if the value is
        a function, DO NOT call it. Otherwise, return the value as-is. """
    if isinstance(x, vtypes.VSymbol):
        return env.get(x)
    else:
        return x


# TODO:
# - dict
#   - 'object already does this, but if we want non-symbol keys, we would
#     benefit from something like
#     dict ["a" 1 "b" 2 "c" add 4 5]
#     so basically a list, then loop pairwise, and add to dict
#   - also remove parent! 
#     - this does mean that we should not add functions to dict, at least not
#       functions defined in its original code block. if you do want that, use
#       a regular object instead.
# - length
# - read-next
# - read-file <filename>
# - read-string <string>
# - read (from stdin)

