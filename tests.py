# tests.py

import os
import unittest
#
import interpreter
import parser
import tokenizer
import vtypes

T = tokenizer.tokenize

class TestTokenizer(unittest.TestCase):
    def test_tokenizer_basics(self):
        self.assertEqual(T("  a b   c  "), ["a", "b", "c"])
        self.assertEqual(T('"ja ne" [x y]'), ['"ja ne"', "[", "x", "y", "]"])
    def test_comments(self):
        self.assertEqual(T("3  #bah\n 4"), ["3", "4"])

class TestParser(unittest.TestCase):
    def test_accessors(self):
        self.assertEqual(vtypes.VSymbol("a/b").is_accessor(), True)
        self.assertEqual(vtypes.VSymbol("a-b").is_accessor(), False)

        parts = vtypes.VSymbol("a/b").split_accessor()
        self.assertEqual(parts, [vtypes.VSymbol("a"), vtypes.VSymbol("b")])

        parts = vtypes.VSymbol("names/0").split_accessor()
        self.assertEqual(parts, [vtypes.VSymbol("names"), vtypes.VInteger(0)])

        # this should remove the trailing ":"
        parts = vtypes.VSymbol("names/0:").split_accessor()
        self.assertEqual(parts, [vtypes.VSymbol("names"), vtypes.VInteger(0)])

class TestTypes(unittest.TestCase):
    pass

class TestInterpreter(unittest.TestCase):
    pass

#
# custom tests

class TestVlinder(unittest.TestCase):
    def setUp(self):
        self.intp = interpreter.Interpreter()

# find the 'tests' directory
whereami = os.path.dirname(os.path.abspath(__file__))
test_dir = os.path.join(whereami, "tests")
# collect all *.txt files in it
test_files = [os.path.join(test_dir, fn) for fn in os.listdir(test_dir)
              if fn.endswith(".txt")]

def _make_test_method(fn, code, result):
    def f(self):
        check_code(fn, code, result)
    return f

test_data = []
for fn in test_files:
    shortname, _ = os.path.splitext(os.path.basename(fn))
    with open(fn, 'r') as f:
        current_test_data = []
        for line in f.readlines():
            line = line.rstrip()
            if line.startswith("=>"):
                test_code = "\n".join(current_test_data).strip()
                t = (shortname, test_code, line[2:].strip())
                test_data.append(t)
                current_test_data = []
            else:
                current_test_data.append(line)

# this code is used for unittest...
for idx, (fn, code, result) in enumerate(test_data):
    f = _make_test_method(fn, code, result)
    method_name = "test_%s_%03d" % (shortname, idx+1)
    setattr(TestVlinder, method_name, f)

# this code will run the tests through nose
def test_generator():
    for fn, code, result in test_data:
        yield check_code, fn, code, result

def check_code(fn, code, result):
    intp = interpreter.Interpreter()
    try:
        results = intp.eval_string(code)
    except:
        print("An error occurred!")
        print(code)
        raise
    assert str(result) == results[-1].to_string(), \
      "Expected %r, got %r instead\nCode:\n%s" % (
              result, results[-1].to_string(), code)

if __name__ == "__main__":
    unittest.main()

