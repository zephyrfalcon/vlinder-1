# Quick overview of Vlinder-1

## What is it?

* next iteration of the Vlinder-0 prototype, which had problems :(
* inspired by Rebol/Red, Lisp/Scheme, Python, Logo, Io, JavaScript
* dynamically typed
* interpreted
* homoiconic (I think :-)
* it's a prototype
    * written in Python 3.6
    * lots of stuff won't be covered, like optimization, a VM, TCO, concurrency, 
      whatever else is necessary to make a language perform and/or be useful in 
      the Real World

## Data types

* the usual suspects: integers, strings, floats (TBI)
* symbols
* functions, of course
* lists and blocks
    * in fact, lists *are* blocks; all lists have a namespace associated with
      them
* objects/modules/dictionaries/namespaces
    * all of these are essentially the same (covered internally by `VObject`)

## What sets it apart?

* much like Red/Rebol, code blocks are **not evaluated** unless you explicitly
  tell the language to do so
    * this allows for "dialects" inside blocks
* unlike Red/Rebol, Vlinder-1 handles namespaces differently, and does not
  implement a variety of specialized type literals (or even types) for email, 
  IP address, filename, etc.
    * new types should be created with the building blocks we have
* first-class namespaces
    * for example, you can access and directly manipulate the caller's
      namespace from inside a function

